## How to run

**Prerequisites**: You'll need [Node](https://nodejs.org) and [PNPM](https://pnpm.io/installation) installed on your local computer.

Run

```
pnpm install
```

to install dependencies, then

```
pnpm start
```

to start the local development server.

To build for production, run

```
pnpm run build
```
